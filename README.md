# OpenML dataset: EaseMyTrip-Flight-Fare-Travel-Listings

https://www.openml.org/d/43788

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was created by our in house Web Scraping and Data Mining teams at PromptCloud and DataStock. You can download the full dataset here. This sample contains 30K records.
Content
Total Records Count : 199244 Domain Name : easemytrip.in Date Range: 01st Apr 2020 - 30th Jun 2020  File Extension :: csv
Available Fields: Uniq Id, Crawl Timestamp, Source, Layover1, Layover2, Layover3, Destination, Flight Operator, Flight Number, Departure Date, Departure Time, Arrival Date, Arrival Time, Total Time, Number Of Stops, Fare
Acknowledgments
We wouldn't be here without the help of our in house web scraping and data mining teams at PromptCloud and DataStock.
Inspiration
This dataset was created keeping in mind our data scientists and researchers across the world.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43788) of an [OpenML dataset](https://www.openml.org/d/43788). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43788/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43788/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43788/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

